FROM ubuntu:18.04
RUN apt-get update
RUN apt-get install -y software-properties-common apt-utils
RUN apt-add-repository ppa:ondrej/php \
  && apt-get update \
  && apt-get -y install sudo \
  && DEBIAN_FRONTEND=noninteractive \
  && apt-get install -y apache2
RUN DEBIAN_FRONTEND=noninteractive apt-get install -y curl php7.0 libapache2-mod-php php7.0-mysql php-mysql php-curl php-gd php-mbstring php-xml php-xmlrpc php-soap php-intl php-zip
RUN cd /tmp && \
  curl -O https://wordpress.org/latest.tar.gz && \
  tar xzvf latest.tar.gz && \
  touch /tmp/wordpress/.htaccess && \
  chmod 660 /tmp/wordpress/.htaccess && \
  cp /tmp/wordpress/wp-config-sample.php /tmp/wordpress/wp-config.php && \
  mkdir -p /var/www/html && \
  cp -a /tmp/wordpress/. /var/www/html && \
  rm /var/www/html/index.html
RUN chown -R www-data:www-data /var/www/html && \
 find /var/www/html -type d -exec chmod g+s {} \;&& \
 chmod g+w /var/www/html/wp-content && \
 chmod -R g+w /var/www/html/wp-content/themes && \
 chmod -R g+w /var/www/html/wp-content/plugins

RUN sed -i "s/database_name_here/wp/" /var/www/html/wp-config.php && \
  sed -i "s/username_here/root/" /var/www/html/wp-config.php && \
  sed -i "s/password_here/rootroot/" /var/www/html/wp-config.php && \
  sed -i "s/localhost/basewp.cpmfusasjbwl.us-east-2.rds.amazonaws.com/" /var/www/html/wp-config.php
CMD apachectl -D FOREGROUND
EXPOSE 80

